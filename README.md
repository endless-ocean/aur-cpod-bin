# aur-cpod-bin

This is a fork of the old `cpod-bin`'s last commit from the AUR repo — the original address is https://aur.archlinux.org/cpod-bin.git

It will simply download the released build of CPod from it's official github repo rather than build it from scratch.

And the commit source is https://aur.archlinux.org/cgit/aur.git/snapshot/aur-4e4060ab0a67d16f1625af4947728ee5599949b8.tar.gz


## Usage

This script only works on Arch Linux and arch-based distros.

1. ```
   git clone https://gitlab.com/endless-ocean/aur-cpod-bin.git
   ```
- Clone this repo to your local directory.
2. ```
   cd aur-cpod-bin && makepkg
   ```
- Simply run this command to build the package.
3. ```
   sudo pacman -Syy electron2 && sudo pacman -U ./cpod-bin-*.pkg.tar.xz
   ```
- Lastly, install it to you system.

## Further maintenance?

Generally these's no need of further change/maintenance unless the original developer is insist to continue the development.


